Name:       testapp
Version:    0.1.0
Release:    1
License:    MIT
Summary:    App for testing the build system
BuildArch:  noarch
Requires:   python >= 3.5

%description
Test package

%prep

%build

%install
make install DESTDIR=%buildroot

%post

%postun

%files
%{_bindir}

%changelog

