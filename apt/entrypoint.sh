#!/usr/bin/env bash

[ -z "$DEBIAN_DIR" ] && { echo "DEBIAN_DIR env var is required but not set"; exit 1; }
[ -z "$SOURCE_DIR" ] && { echo "SOURCE_DIR env var is required but not set"; exit 1; }
[ -z "$BUILD_ROOT" ] && BUILD_ROOT="/tmp/buildroot"
[ -z "$OUTPUT_DIR" ] && OUTPUT_DIR="$SOURCE_DIR/pkg"

mkdir -p "$BUILD_ROOT"/source/debian
cp -r "$SOURCE_DIR"/* "$BUILD_ROOT"/source
cp -r "$DEBIAN_DIR"/* "$BUILD_ROOT"/source/debian
pushd "$BUILD_ROOT" || exit
pushd source || exit
debuild -us -uc
popd || exit

mkdir -p "$OUTPUT_DIR"
cp {*.dsc,*.tar.gz,*.deb,*.build,*.buildinfo,*.changes} "$OUTPUT_DIR"
