##
# packager
#
# @file
# @version 0.1

.PHONY: run
run:
	podman run -it --rm -v $PWD:/work kube.cat/cocainefarm/packager:latest

# end
