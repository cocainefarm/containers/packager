#!/usr/bin/env bash
shopt -s globstar

[ -z "$SPEC_FILE" ] && { echo "SPEC_FILE env var is required but not set"; exit 1; }
[ -z "$SOURCE_DIR" ] && { echo "SOURCE_DIR env var is required but not set"; exit 1; }
[ -z "$RPMBUILD_ROOT" ] && RPMBUILD_ROOT="/root/rpmbuild"
[ -z "$BUILD_DIR" ] && BUILD_DIR="$RPMBUILD_ROOT/BUILD"
[ -z "$OUTPUT_DIR" ] && OUTPUT_DIR="$SOURCE_DIR/pkg"

rpmdev-setuptree
cp -r "$SOURCE_DIR"/* "$BUILD_DIR"
dnf builddep -y "$SPEC_FILE"
spectool -R -g "$SPEC_FILE"
rpmbuild -ba "$SPEC_FILE"

mkdir -p "$OUTPUT_DIR"
cp "$RPMBUILD_ROOT/RPMS"/*/* "$OUTPUT_DIR"
cp "$RPMBUILD_ROOT/SRPMS"/* "$OUTPUT_DIR"
